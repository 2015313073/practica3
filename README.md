# Practica3
Proyecto Unidad I para la materia Desarrollo Web Profesional

# Autor
Moises Navarro Galvan - IDGS05 - 8vo Cuatrimestre de Ingenieria en Desarrollo y Gestion de Software

# instalacion y uso de node 18.14.1
nvm install 18.14.1
nvm use 18.14.1

# Instalacion de librerias react
npm i react react-dom react-router-dom @babel/core @babel/preset-env @babel/preset-react webpack webpack-cli webpack-dev-server babel-loader html-loader html-webpack-plugin mini-css-extract-plugin css-loader style-loader sass sass-loader -D

## Licencia
MIT

## Estatus
En construccion

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/2015313073/practica3/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
